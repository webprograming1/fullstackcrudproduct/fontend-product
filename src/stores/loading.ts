import { ref } from "vue";
import { defineStore } from "pinia";

export const useLoadingStore = defineStore("Loading", () => {
  const isLoading = ref(false);

  return { isLoading };
});
